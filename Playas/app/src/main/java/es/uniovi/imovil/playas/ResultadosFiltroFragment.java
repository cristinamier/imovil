package es.uniovi.imovil.playas;


import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.telecom.Call;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;

import static es.uniovi.imovil.playas.PlayaListFragment.URL;
import static es.uniovi.imovil.playas.PlayaListFragment.apiKey;


public class ResultadosFiltroFragment extends Fragment implements AdapterView.OnItemClickListener{

    Callback mCallback;
    private static final String PLAYAS_ARG = "playas";
    View rootView;
    ListView lvResultados;
    private PlayaAdapter mAdapter = null;
    private ArrayList<Playa> playas = null;
    ArrayList<Playa> listaPlayas = null;
    RequestQueue requestQueue;
    TextView textViewActualizando;
    public static final String bPlayasResultados = "playas";
    int contador;

    public static ResultadosFiltroFragment newInstance(ArrayList<Playa> playas){

        ResultadosFiltroFragment fragment = new ResultadosFiltroFragment();

        Log.i("FRAGMENTO", "Dentro de newInstance");
        Bundle args = new Bundle();
        args.putParcelableArrayList(PLAYAS_ARG, playas);
        fragment.setArguments(args);

        return fragment;

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        rootView = inflater.inflate(R.layout.resultados_filtro_fragment, container, false);
        lvResultados = (ListView) rootView.findViewById(R.id.list_view_resultados);
        Bundle args = getArguments();
        Log.i("ResultadosFiltroFrag", "");
        if (savedInstanceState != null) {
            playas = savedInstanceState.getParcelableArrayList(bPlayasResultados);
            mAdapter = new PlayaAdapter(getActivity(), playas);
            lvResultados.setAdapter(mAdapter);
            lvResultados.setOnItemClickListener(this);

            System.out.println("**********PLAYAS GUARDADAS********");
        }
        else if(args != null){
            playas = args.getParcelableArrayList(PLAYAS_ARG);
            for(int j = 0; j < playas.size(); j++){
                Log.i("FILTRO", String.valueOf(playas.get(j).getNombre()));
            }
            Log.i("ResultadosFiltroFrag", "Longitud de playas: " + playas.size());

            mAdapter = new PlayaAdapter(this.getActivity(), playas);
            lvResultados.setAdapter(mAdapter);
            lvResultados.setOnItemClickListener(this);

            // Hacer peticiones
            requestQueue = SingletonVolley.getInstance(getContext()).getRequestQueue();
            String url = URL + String.valueOf(playas.get(0).getCodigo() + apiKey);

            Log.i("FRAGMENTO", url);

            // Si la predicción ha cambiado (después de guardar en un fichero los datos)
            contador = 0;
            textViewActualizando = (TextView) rootView.findViewById(R.id.textViewActualizandoResultados);
            textViewActualizando.setVisibility(View.VISIBLE);
            listaPlayas = new ArrayList<Playa>();
            parseConsultaPlaya(url);

        }

        return rootView;
    }

    @Override
    public void onSaveInstanceState(Bundle outstate){

        outstate.putParcelableArrayList(bPlayasResultados, playas);
        super.onSaveInstanceState(outstate);

    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id){

        final Playa playa = (Playa) parent.getItemAtPosition(position);
        //Log.i("FRAGMENT", playa.getNombre() + " " + playa.getCodigo() + " " + playa.getPrediccion().getCieloManana());
        if(playa.getPrediccion() == null){
            consulta(URL + String.valueOf(playa.getCodigo()) + apiKey, new FilterFragment.Callback() {
                @Override
                public void onSuccess(Prevision p) {
                    playa.setPrediccion(p);
                    mCallback.onPlayaSelected(playa);
                    return;
                }
            });
        }

        else {
            mCallback.onPlayaSelected(playa);
        }

    }

    public interface Callback{

        public void onPlayaSelected(Playa playa);
    }

    @Override
    public void onAttach(Activity activity){

        super.onAttach(activity);
        try{
            mCallback = (Callback) activity;
        } catch(ClassCastException e){
            throw new ClassCastException(activity.toString() + " must implement Callbacks");
        }

    }

    public void parseConsultaPlaya(final String url){

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET,
                url, null, new Response.Listener<JSONObject>() {

            @Override
            public void onResponse(JSONObject response) {
                try {
                    String urlPlaya = String.valueOf(response.getString("datos"));
                    JsonArrayRequest jsonArrayRequest = new JsonArrayRequest(Request.Method.GET,
                            urlPlaya, null, new Response.Listener<JSONArray>() {
                        @Override
                        public void onResponse(JSONArray response) {
                            try {
                                JSONObject datos = response.getJSONObject(0);
                                Gson gson = new Gson();
                                ConsultaPlaya consulta = gson.fromJson(datos.toString(), ConsultaPlaya.class);
                                //consultas.add(consulta);
                                System.out.println(consulta.getPrediccion());
                                Dia dia = consulta.getPrediccion().getDia().get(0);
                                Prevision prevision = new Prevision(dia.getEstadoCielo().getF1(),
                                        dia.getEstadoCielo().getDescripcion1(), dia.getEstadoCielo().getF2(),
                                        dia.getEstadoCielo().getDescripcion2(), dia.getViento().getDescripcion1(),
                                        dia.getViento().getDescripcion2(), dia.getOleaje().getDescripcion1(), dia.getOleaje().getDescripcion2(),
                                        dia.gettMaxima().getValor1(), dia.getsTermica().getDescripcion1(), dia.gettAgua().getValor1(),
                                        dia.getUvMax().getValor1());
                                //Playa p = new Playa(playas.get(contador).getNombre(), playas.get(contador).getLocalidad(), playas.get(contador).getCodigo(), playas.get(contador).getCoordenadas(), prevision);
                                //listaPlayas.add(p);

                                playas.get(contador).setPrediccion(prevision);
                                contador++;
                                if(contador < playas.size()){
                                    //System.out.println(consultas.size() + consulta.getNombre());

                                    String url = URL + playas.get(contador).getCodigo()  + apiKey;
                                    parseConsultaPlaya(url);
                                }
                                else if(contador == playas.size()){
                                    // Ordenar y mostrar lista
                                    Log.i("FRAGMENTO", "Longitud de playas después de hacer las consultas" + playas.size());
                                    actualizarLista();
                                }



                            } catch (JSONException e) {
                                e.printStackTrace();
                                AlertDialog.Builder ab = new AlertDialog.Builder(getContext());
                                // TODO: cadenas en res
                                ab.setTitle("Error");
                                ab.setMessage(R.string.problemaConexion);
                                ab.setPositiveButton("OK", new DialogInterface.OnClickListener(){
                                    public void onClick(DialogInterface dialog, int which){
                                        getActivity().finish();
                                    }
                                });
                                AlertDialog alerta = ab.create();
                                alerta.show();

                            }
                        }
                    }, new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            error.printStackTrace();
                        }
                    });

                    requestQueue.add(jsonArrayRequest);

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
                AlertDialog.Builder ab = new AlertDialog.Builder(getContext());
                // TODO: cadenas en res
                ab.setTitle("Error");
                ab.setMessage(R.string.problemaConexion);
                ab.setPositiveButton("OK", new DialogInterface.OnClickListener(){
                    public void onClick(DialogInterface dialog, int which){
                        getActivity().finish();
                    }
                });
                AlertDialog alerta = ab.create();
                alerta.show();
            }

        });
        requestQueue.add(jsonObjectRequest);

    }

    public void actualizarLista(){
        /*for(int i = 0; i < playas.size(); i++){
            System.out.println(playas.get(i).getNombre() + " (" + playas.get(i).getLocalidad() + ")\n" +
                "Cielo: " + playas.get(i).getPrediccion().getTemperatura() +
                "\n______________________\n");
        }*/

        // Ordenar playas según temperatura y cielo
        for(int i = 0; i < playas.size()-1; i++){
            for(int j = i+1; j < playas.size(); j++){
                Log.i("FRAGMENTO", String.valueOf(i) + " " + String.valueOf(j));
                int cielo_i = playas.get(i).getPrediccion().getCieloMananaCodigo();
                int cielo_j = playas.get(j).getPrediccion().getCieloMananaCodigo();
                int temperatura_i = playas.get(i).getPrediccion().getTemperatura();
                int temperatura_j = playas.get(j).getPrediccion().getTemperatura();

                if(temperatura_i < temperatura_j && cielo_i >= cielo_j ){
                    Playa p = playas.get(i);
                    playas.remove(i);
                    if(i < playas.size()-1)
                        playas.add(i, playas.get(j-1));
                    else
                        playas.add(playas.get(j-1));
                    playas.remove(j);
                    if(j < playas.size()-1)
                        playas.add(j, p);
                    else
                        playas.add(p);
                }
            }
        }

        System.out.println("PLAYAS ORDENADAS SEGÚN  CIELO Y TEMPERATURA");
        /*Arrays.fill(nombres, null);
        Arrays.fill(localidades, null);
        for(int i = 0; i < playas.size(); i++){
            System.out.println(playas.get(i).getNombre() + " (" + playas.get(i).getLocalidad() + ")\n" +
                    "Cielo: " + playas.get(i).getPrediccion().getCieloManana() +
                    "\nTemperatura" + playas.get(i).getPrediccion().getTemperatura() +
                    "\n______________________\n");
            nombres[i] = playas.get(i).getNombre();
            localidades[i] = playas.get(i).getLocalidad();

        }*/


        textViewActualizando.setVisibility(View.INVISIBLE);
        mAdapter = new PlayaAdapter(getActivity(), playas);
        lvResultados.setAdapter(mAdapter);
        lvResultados.setOnItemClickListener(this);

    }


    public void consulta(final String url, final FilterFragment.Callback onCallback){

        requestQueue = SingletonVolley.getInstance(getContext()).getRequestQueue();

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET,
                url, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                    String urlPlaya = String.valueOf(response.getString("datos"));
                    JsonArrayRequest jsonArrayRequest = new JsonArrayRequest(Request.Method.GET,
                            urlPlaya, null, new Response.Listener<JSONArray>() {
                        @Override
                        public void onResponse(JSONArray response) {
                            try {
                                JSONObject datos = response.getJSONObject(0);
                                Gson gson = new Gson();
                                ConsultaPlaya consulta = gson.fromJson(datos.toString(), ConsultaPlaya.class);
                                Dia dia = consulta.getPrediccion().getDia().get(0);
                                Prevision prevision = new Prevision(dia.getEstadoCielo().getF1(),
                                        dia.getEstadoCielo().getDescripcion1(), dia.getEstadoCielo().getF2(),
                                        dia.getEstadoCielo().getDescripcion2(), dia.getViento().getDescripcion1(),
                                        dia.getViento().getDescripcion2(), dia.getOleaje().getDescripcion1(), dia.getOleaje().getDescripcion2(),
                                        dia.gettMaxima().getValor1(), dia.getsTermica().getDescripcion1(), dia.gettAgua().getValor1(),
                                        dia.getUvMax().getValor1());
                                onCallback.onSuccess(prevision);
                            } catch (JSONException e) {
                                e.printStackTrace();
                                AlertDialog.Builder ab = new AlertDialog.Builder(getContext());
                                // TODO: cadenas en res
                                ab.setTitle("Error");
                                ab.setMessage(R.string.problemaConexion);
                                ab.setPositiveButton("OK", new DialogInterface.OnClickListener(){
                                    public void onClick(DialogInterface dialog, int which){
                                        getActivity().finish();
                                    }
                                });
                                AlertDialog alerta = ab.create();
                                alerta.show();
                            }
                        }
                    }, new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            error.printStackTrace();
                        }
                    });
                    requestQueue.add(jsonArrayRequest);
                } catch (JSONException e) {
                    e.printStackTrace();
                    AlertDialog.Builder ab = new AlertDialog.Builder(getContext());
                    // TODO: cadenas en res
                    ab.setTitle("Error");
                    ab.setMessage(R.string.problemaConexion);
                    ab.setPositiveButton("OK", new DialogInterface.OnClickListener(){
                        public void onClick(DialogInterface dialog, int which){
                            getActivity().finish();
                        }
                    });
                    AlertDialog alerta = ab.create();
                    alerta.show();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
            }
        });
        requestQueue.add(jsonObjectRequest);
    }

    public void listarPlayas(ArrayList<Playa> p){
        playas = p;
        for(int j = 0; j < playas.size(); j++){
            Log.i("FILTRO", String.valueOf(playas.get(j).getNombre()));
        }
        Log.i("ResultadosFiltroFrag", "Longitud de playas: " + playas.size());

        mAdapter = new PlayaAdapter(this.getActivity(), playas);
        lvResultados.setAdapter(mAdapter);
        lvResultados.setOnItemClickListener(this);

        // Hacer peticiones
        requestQueue = SingletonVolley.getInstance(getContext()).getRequestQueue();
        String url = URL + String.valueOf(playas.get(0).getCodigo() + apiKey);

        Log.i("FRAGMENTO", url);

        // Si la predicción ha cambiado (después de guardar en un fichero los datos)
        contador = 0;
        textViewActualizando = (TextView) rootView.findViewById(R.id.textViewActualizandoResultados);
        textViewActualizando.setVisibility(View.VISIBLE);
        listaPlayas = new ArrayList<Playa>();
        parseConsultaPlaya(url);
    }

}
