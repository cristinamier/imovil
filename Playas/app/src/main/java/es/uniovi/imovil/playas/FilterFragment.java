package es.uniovi.imovil.playas;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.Parcelable;
import android.provider.MediaStore;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.RadioButton;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationServices;
import com.google.gson.Gson;
import com.google.gson.JsonObject;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Text;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;

import static es.uniovi.imovil.playas.PlayaListFragment.apiKey;

/**
 * Created by cristina on 4/5/17.
 */

public class FilterFragment extends Fragment {

    RadioButton rbPlaya;
    RadioButton rbLocalidad;
    RadioButton rbProximidad;
    AutoCompleteTextView tvPlaya;
    AutoCompleteTextView tvLocalidad;
    SeekBar seekBarProximidad;
    TextView tvProximidad;
    Button bResultado;
    RequestQueue requestQueue;
    Coordenadas[] coordenadas;
    long[] codigos;
    CallbackLocalidad callbackLocalidad = null;
    ArrayList<Playa> playasEncontradas;
    static Location localizacion;

    int contador;
    //static Location localizacionActual;

    public static FilterFragment newInstance(Location l){
        FilterFragment fragment = new FilterFragment();
        localizacion = l;

        return fragment;
    }

    public FilterFragment(){

    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try{
            callbackLocalidad = (CallbackLocalidad) activity;
        } catch(ClassCastException e){
            throw new ClassCastException(activity.toString() + " must implement Callbacks");
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState){
        View rootView;
        rootView = inflater.inflate(R.layout.filter_fragment, container, false);
        contador = 0;

        // Si estamos restaurando desde un estado previo no hacemos nada
        if(savedInstanceState != null){
            return rootView;
        }

        rbPlaya = (RadioButton) rootView.findViewById(R.id.radioButtonPlaya);
        rbLocalidad = (RadioButton) rootView.findViewById(R.id.radioButtonLocalidad);
        rbProximidad = (RadioButton) rootView.findViewById(R.id.radioButtonProximidad);
        tvPlaya = (AutoCompleteTextView) rootView.findViewById(R.id.autoCompleteTextViewPlaya);
        tvLocalidad = (AutoCompleteTextView) rootView.findViewById(R.id.autoCompleteTextViewLocalidad);
        seekBarProximidad = (SeekBar) rootView.findViewById(R.id.seekBarProximidad);
        seekBarProximidad.setEnabled(false);
        tvProximidad = (TextView) rootView.findViewById(R.id.textViewProximidad);
        bResultado = (Button) rootView.findViewById(R.id.buttonResultados);

        // Listado de playas para el autoCompleteTextView
        final String[] optPlayas = getResources().getStringArray(R.array.playas);

        // Listado de localidades para el autoCompleteTextView
        final ArrayList<String> localidadesFiltro = new ArrayList<>(Arrays.asList(getResources().getStringArray(R.array.localidades)));
        // Listado de localidades para crear con los resultados de los filtros las playas a mostrar
        final ArrayList<String> localidades = new ArrayList<>(Arrays.asList(getResources().getStringArray(R.array.localidades)));
        // Se eliminan los elementos repetidos de localidadesFiltro, que pasa a llamarse optLocalidades
        HashSet<String> hs = new HashSet<>();
        hs.addAll(localidades);
        localidades.clear();
        localidades.addAll(hs);
        String[] optLocalidades = new String[localidades.size()];
        localidades.toArray(optLocalidades);


        ArrayAdapter<String> adaptadorPlayas = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_expandable_list_item_1, optPlayas);
        tvPlaya.setAdapter(adaptadorPlayas);
        final ArrayAdapter<String> adaptadorLocalidades = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_expandable_list_item_1, optLocalidades);
        tvLocalidad.setAdapter(adaptadorLocalidades);

        // Se crean dos arrays, uno para las coordenadas y otro con los códigos
        // (necesarios para la búsqueda en los filtros)
        int longitud_coordenadas = optPlayas.length;
        coordenadas = new Coordenadas[longitud_coordenadas];
        System.arraycopy(PlayaListFragment.coordenadas_oeste, 0, coordenadas, 0, PlayaListFragment.coordenadas_oeste.length);
        System.arraycopy(PlayaListFragment.coordenadas_centro, 0, coordenadas, PlayaListFragment.coordenadas_oeste.length, PlayaListFragment.coordenadas_centro.length);
        System.arraycopy(PlayaListFragment.coordenadas_este, 0, coordenadas, PlayaListFragment.coordenadas_centro.length + PlayaListFragment.coordenadas_oeste.length, PlayaListFragment.coordenadas_este.length);

        int longitud_codigos = PlayaListFragment.codigos_centro.length + PlayaListFragment.codigos_este.length + PlayaListFragment.codigos_oeste.length;
        codigos = new long[longitud_codigos];

        // System.arraycopy(array que quiero meter, desde que posicion quiero copiar, a que array lo quiero meter, desde que posicion lo quiero meter, longitud del array copiado)
        System.arraycopy(PlayaListFragment.codigos_oeste, 0, codigos, 0, PlayaListFragment.codigos_oeste.length);
        System.arraycopy(PlayaListFragment.codigos_centro, 0, codigos, PlayaListFragment.codigos_oeste.length, PlayaListFragment.codigos_centro.length);
        System.arraycopy(PlayaListFragment.codigos_este, 0, codigos, PlayaListFragment.codigos_centro.length + PlayaListFragment.codigos_oeste.length, PlayaListFragment.codigos_este.length);


        // Para que solo esté seleccionado un radiobutton
        rbPlaya.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked){
                    rbLocalidad.setChecked(false);
                    rbProximidad.setChecked(false);
                    tvPlaya.setEnabled(true);
                    tvLocalidad.setEnabled(false);
                    seekBarProximidad.setEnabled(false);
                    tvProximidad.setEnabled(false);
                }
            }
        });

        rbLocalidad.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked){
                    rbPlaya.setChecked(false);
                    rbProximidad.setChecked(false);
                    tvPlaya.setEnabled(false);
                    tvLocalidad.setEnabled(true);
                    seekBarProximidad.setEnabled(false);
                    tvProximidad.setEnabled(false);
                }
            }
        });

        rbProximidad.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked){
                    rbPlaya.setChecked(false);
                    rbLocalidad.setChecked(false);
                    tvPlaya.setEnabled(false);
                    tvLocalidad.setEnabled(false);
                    seekBarProximidad.setEnabled(true);
                    tvProximidad.setEnabled(true);
                }
            }
        });

        seekBarProximidad.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                tvProximidad.setText(String.valueOf(progress) + " km");
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });

        bResultado.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (rbPlaya.isChecked()) {
                    // Abrir predicción de esa playa
                    final String nombre;
                    final String localidad;
                    final Coordenadas coordenada;
                    Prevision prediccion;

                    // Buscar en qué posición del array de playas está
                    int i = 0;
                    boolean encontrado = false;
                    String[] playas = getResources().getStringArray(R.array.playas);
                    while (i < playas.length && !encontrado) {
                        if (playas[i].equals(tvPlaya.getText().toString())) {
                            encontrado = true;
                        }
                        i++;
                    }

                    // Si existe la playa se muestra la predicción
                    if (encontrado) {
                        nombre = tvPlaya.getText().toString();

                        // Buscar el código
                        final long codigo = codigos[i - 1];

                        // Buscar la localidad
                        String[] localidades = getResources().getStringArray(R.array.localidades);
                        localidad = localidades[i - 1];

                        // Buscar las coordenadas
                        coordenada = coordenadas[i - 1];


                        // Llamar a PlayaDetailsActivity
                        // Hacer la petición
                        consulta(PlayaListFragment.URL + String.valueOf(codigo) + apiKey, new Callback() {
                            @Override
                            public void onSuccess(Prevision p) {
                                // Crear una playa con nombre, localidad, coordenadas y predicción
                                Playa playa = new Playa(nombre, localidad, codigo, coordenada, p);
                                Intent intent = new Intent(getContext(), PlayaDetailsActivity.class);
                                intent.putExtra(PlayaDetailsActivity.PLAYA, (Parcelable)playa);
                                startActivity(intent);
                                getActivity().finish();
                            }
                        });
                    } else {
                        Toast.makeText(getContext(), R.string.noExistePlaya, Toast.LENGTH_SHORT).show();
                    }


                } else if (rbLocalidad.isChecked()) {

                    // Buscar en todas las veces que existe esa localidad
                    int i = 0;
                    ArrayList<Integer> localidadesEncontradas = new ArrayList<Integer>();
                    playasEncontradas = new ArrayList<Playa>();
                    boolean encontrado = false;
                    while (i < localidadesFiltro.size()) {
                        if (localidadesFiltro.get(i).equals(tvLocalidad.getText().toString())) {
                            localidadesEncontradas.add(i);
                            playasEncontradas.add(new Playa(optPlayas[i], localidadesFiltro.get(i), codigos[i], coordenadas[i], null));
                            encontrado = true;
                        }
                        i++;
                    }

                    if (encontrado) {
                        // Mostrar la lista con esas playas
                        callbackLocalidad.onSuccess(playasEncontradas);
                        getActivity().finish();
                    } else {
                        Toast.makeText(getContext(), R.string.noExisteLocalidad, Toast.LENGTH_SHORT).show();
                    }
                } else if (rbProximidad.isChecked()) {

                    // Buscar en todas las playas que están a una distancia menor o igual que la introducida por el usuario
                    int i = 0;
                    playasEncontradas = new ArrayList<Playa>();
                    boolean encontrado = false;

                    if(localizacion != null) {
                        Coordenadas puntoActual = new Coordenadas(localizacion.getLatitude(), localizacion.getLongitude());
                        while (i < coordenadas.length) {
                            double distancia = calculaDistancia(puntoActual, coordenadas[i]);
                            if (distancia <= seekBarProximidad.getProgress()) {
                                Playa p = new Playa(optPlayas[i], localidadesFiltro.get(i), codigos[i], coordenadas[i], null);
                                playasEncontradas.add(p);
                                encontrado = true;
                            }
                            i++;
                        }
                    }
                    else{
                        Toast.makeText(getContext(), R.string.gpsDesactivado, Toast.LENGTH_SHORT).show();
                        return;
                    }

                    if (encontrado) {
                        // Mostrar la lista con esas playas
                        callbackLocalidad.onSuccess(playasEncontradas);
                        getActivity().finish();
                    }else{
                        Toast.makeText(getContext(), R.string.noResultados, Toast.LENGTH_SHORT).show();
                    }
                }
            }

            }
        );

        return rootView;
    }



    private double calculaDistancia(Coordenadas punto1, Coordenadas punto2){

        Location l1 = new Location("");
        l1.setLatitude(punto1.getLatitud());
        l1.setLongitude(punto1.getLongitud());

        Location l2 = new Location("");
        l2.setLatitude(punto2.getLatitud());
        l2.setLongitude(punto2.getLongitud());

        return l2.distanceTo(l1) / 1000.0;
    }

    public void consulta(final String url, final Callback onCallback){

        requestQueue = SingletonVolley.getInstance(getContext()).getRequestQueue();

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET,
                url, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                    String urlPlaya = String.valueOf(response.getString("datos"));
                    JsonArrayRequest jsonArrayRequest = new JsonArrayRequest(Request.Method.GET,
                            urlPlaya, null, new Response.Listener<JSONArray>() {
                        @Override
                        public void onResponse(JSONArray response) {
                            try {
                                JSONObject datos = response.getJSONObject(0);
                                Gson gson = new Gson();
                                ConsultaPlaya consulta = gson.fromJson(datos.toString(), ConsultaPlaya.class);
                                Dia dia = consulta.getPrediccion().getDia().get(0);
                                Prevision prevision = new Prevision(dia.getEstadoCielo().getF1(),
                                        dia.getEstadoCielo().getDescripcion1(), dia.getEstadoCielo().getF2(),
                                        dia.getEstadoCielo().getDescripcion2(), dia.getViento().getDescripcion1(),
                                        dia.getViento().getDescripcion2(), dia.getOleaje().getDescripcion1(), dia.getOleaje().getDescripcion2(),
                                        dia.gettMaxima().getValor1(), dia.getsTermica().getDescripcion1(), dia.gettAgua().getValor1(),
                                        dia.getUvMax().getValor1());
                                onCallback.onSuccess(prevision);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    }, new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            error.printStackTrace();
                        }
                    });
                    requestQueue.add(jsonArrayRequest);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
            }
        });
        requestQueue.add(jsonObjectRequest);
    }

    public interface Callback{
        void onSuccess(Prevision p);
    }

    public interface CallbackLocalidad{
        void onSuccess(ArrayList<Playa> playas);
    }

}
