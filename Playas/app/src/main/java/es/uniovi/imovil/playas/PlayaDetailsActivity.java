package es.uniovi.imovil.playas;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.widget.Toast;
import android.view.MenuItem;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;

/**
 * Created by cristina on 5/4/17.
 */

public class PlayaDetailsActivity extends ActionBarActivity{

    public static final String PLAYA = "es.uniovi.imovil.cristina.playa.PLAYA";
    private GoogleMap mMap;
    Playa playa;

    @Override
    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.playa_details_activity);

        // Crear el fragmento pasándole el parámetro
        Intent intent = getIntent();
        playa = intent.getParcelableExtra(PLAYA);
        // Existe el contenedor del fragmento?
        if(findViewById(R.id.fragment_container) != null){

            // Si estamos restaurando desde un estado previo
            if(savedInstanceState != null){
                playa = savedInstanceState.getParcelable(PLAYA);
            }

            PlayaDetailsFragment fragment = PlayaDetailsFragment.newInstance(playa);

            // Añadir el fragmento al contenedor 'fragment_container'
            getSupportActionBar().setTitle(playa.getNombre());
            getSupportFragmentManager().beginTransaction().add(R.id.fragment_container, fragment).commit();
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        }
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        outState.putParcelable(PLAYA, playa);
        super.onSaveInstanceState(outState);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Sin esto, cuando se está viendo una playa desde la lista de resultados del filtro,
        // si se da al botón Up, volvería a la lista inicial y no a la del filtro.
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
        }
        return false;
    }

}
