package es.uniovi.imovil.playas;

import android.os.Parcel;
import android.os.Parcelable;

import java.io.Serializable;

/**
 * Created by cristina on 1/5/17.
 */

public class Coordenadas  implements Parcelable, Serializable{

    double latitud;
    double longitud;

    public Coordenadas(double latitud, double longitud) {
        this.latitud = latitud;
        this.longitud = longitud;
    }

    public double getLatitud() {
        return latitud;
    }

    public double getLongitud() {
        return longitud;
    }

    public Coordenadas(Parcel parcel){

        readFromParcel(parcel);

    }

    @Override
    public int describeContents(){

        return 0;

    }

    @Override
    public void writeToParcel(Parcel dest, int flags){

        dest.writeDouble(latitud);
        dest.writeDouble(longitud);

    }

    public void readFromParcel(Parcel parcel){

        latitud = parcel.readDouble();
        longitud = parcel.readDouble();

    }

    public static final Parcelable.Creator<Coordenadas> CREATOR =
            new Parcelable.Creator<Coordenadas>(){

                public Coordenadas createFromParcel(Parcel in){

                    return new Coordenadas(in);

                }

                public Coordenadas[] newArray(int size){

                    return new Coordenadas[size];

                }

            };

}
