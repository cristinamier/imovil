package es.uniovi.imovil.playas;

import android.os.Parcel;
import android.os.Parcelable;

import java.io.Serializable;

/**
 * Created by cristina on 29/4/17.
 */

public class Playa implements Parcelable, Serializable{

    String nombre;
    String localidad;
    //ConsultaPlaya.Prevision prediccion;
    long codigo;
    Coordenadas coordenadas;
    Prevision prediccion;

    public Playa(String nombre, String localidad, long codigo, Coordenadas coordenadas, Prevision prediccion) {
        this.nombre = nombre;
        this.localidad = localidad;
        this.codigo = codigo;
        this.coordenadas = coordenadas;
        this.prediccion = prediccion;
    }

    public String getNombre(){
        return nombre;
    }

    public String getLocalidad(){
        return localidad;
    }

    public long getCodigo() { return codigo; }

    public Coordenadas getCoordenadas() { return coordenadas; }

    public Prevision getPrediccion(){
        return prediccion;
    }

    public void setPrediccion(Prevision p){ prediccion = p; }

    public Playa(Parcel parcel){

        readFromParcel(parcel);

    }

    @Override
    public int describeContents(){

        return 0;

    }

    @Override
    public void writeToParcel(Parcel dest, int flags){

        dest.writeString(nombre);
        dest.writeString(localidad);
        dest.writeLong(codigo);
        dest.writeValue(coordenadas);
        dest.writeValue(prediccion);

    }

    public void readFromParcel(Parcel parcel){

        nombre = parcel.readString();
        localidad = parcel.readString();
        codigo = parcel.readLong();
        coordenadas = (Coordenadas) parcel.readValue(Coordenadas.class.getClassLoader());
        prediccion = (Prevision) parcel.readValue(Prevision.class.getClassLoader());

    }

    public static final Parcelable.Creator<Playa> CREATOR =
            new Parcelable.Creator<Playa>(){

                public Playa createFromParcel(Parcel in){

                    return new Playa(in);

                }

                public Playa[] newArray(int size){

                    return new Playa[size];

                }

            };

}
