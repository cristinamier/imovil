package es.uniovi.imovil.playas;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInput;
import java.io.ObjectInputStream;
import java.io.ObjectOutput;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import static java.lang.Math.abs;

/**
 * Created by cristina on 4/4/17.
 */

public class PlayaListFragment extends Fragment implements AdapterView.OnItemClickListener{

    public static String URL = "https://opendata.aemet.es/opendata/api/prediccion/especifica/playa/";
    public static String apiKey = "/?api_key=eyJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJjcmlzdGluYW1pZXJkZWxjYXNhckBnbWFpbC5jb20iLCJqdGkiOiIwNjVhYmFhNS02OGJjLTRmZWQtYTg4My1iNWNhNTkwNzlmZTciLCJleHAiOjE1MDIzNzY4ODYsImlzcyI6IkFFTUVUIiwiaWF0IjoxNDk0NjAwODg2LCJ1c2VySWQiOiIwNjVhYmFhNS02OGJjLTRmZWQtYTg4My1iNWNhNTkwNzlmZTciLCJyb2xlIjoiIn0.gqE5EtsljpnD9htZGnQzuQxMtwXn4w4HgTgW4UtXva8";
    public static final String PLAYAS_OESTE_LIST_FILENAME = "playa_oeste_list";
    public static final String PLAYAS_CENTRO_LIST_FILENAME = "playa_centro_list";
    public static final String PLAYAS_ESTE_LIST_FILENAME = "playa_este_list";
    public static final String bPlayas = "playas";
    public static final String FECHA_OESTE = "fecha_oeste";
    public static final String FECHA_CENTRO = "fecha_centro";
    public static final String FECHA_ESTE = "fecha_este";

    static long codigos_oeste[] = {3301704, 3307007, 3302302, 3301804, 3304101, 3303402, 3303407, 3302117,
            3302120};
    static long codigos_centro[] = {3303902, 3306901, 3301608, 3302509, 3301403, 3301406, 3302403};
    static long codigos_este[] = {3307606, 3301903, 3301303, 3305601, 3305602, 3303612, 3303614, 3303618,
            3303626, 3305501};

    static Coordenadas coordenadas_oeste[] = {new Coordenadas(43.55349, -6.99576), new Coordenadas(43.563943, -6.948509),
            new Coordenadas(43.56425, -6.828), new Coordenadas(43.56015, -6.73209), new Coordenadas(43.555655, -6.722276),
            new Coordenadas(43.55309, -6.59716), new Coordenadas(43.54643, -6.53837), new Coordenadas(43.57884, -6.22011),
            new Coordenadas(43.56453, -6.18882)};
    static Coordenadas coordenadas_centro[] = {new Coordenadas(43.55483, -6.11672), new Coordenadas(43.56366, -6.07141),
            new Coordenadas(43.57909, -5.95777), new Coordenadas(43.61865, -5.78786), new Coordenadas(43.58951, -5.76306),
            new Coordenadas(43.5697, -5.71843), new Coordenadas(43.54108, -5.65229)};
    static Coordenadas coordenadas_este[] = {new Coordenadas(43.53343, -5.3797), new Coordenadas(43.480115, -5.225197),
            new Coordenadas(43.47466, -5.17456), new Coordenadas(43.47796, -5.13242), new Coordenadas(43.465255, -5.07),
            new Coordenadas(43.44108, -4.83965), new Coordenadas(43.43787, -4.82789), new Coordenadas(43.432323, -4.81),
            new Coordenadas(43.415772, -4.744592), new Coordenadas(43.391951, -4.578038)};


    long[] codigos = null;
    String[] nombres = null;
    String[] localidades = null;
    Coordenadas[] coordenadas = null;
    int contador;
    String zona;
    CharSequence[] items = null;

    ArrayList<ConsultaPlaya> consultas = new ArrayList<ConsultaPlaya>();
    ArrayList<Playa> playas = new ArrayList<Playa>();
    RequestQueue requestQueue;
    View rootView;
    ListView lvItems;
    TextView textViewActualizando;
    int elementoSeleccionado;
    int actual;

    private SharedPreferences prefs = null;
    SharedPreferences.Editor prefsEditor = null;

    public interface Callbacks {
        public void onPlayaSelected(Playa playa);
    }

    PlayaAdapter mAdapter = null;
    private Callbacks mCallback = null;

    public PlayaListFragment(){

    }

    @Override
    public void onAttach(Activity activity)
    {

        super.onAttach(activity);
        try{
            mCallback = (Callbacks) activity;
        } catch(ClassCastException e){
            throw new ClassCastException(activity.toString() + " must implement Callbacks");
        }

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, final Bundle savedInstanceState) {

        rootView = inflater.inflate(R.layout.playa_list_fragment, container, false);
        prefs = getActivity().getSharedPreferences(MainActivity.PREFERENCES, Context.MODE_PRIVATE);
        prefsEditor = prefs.edit();
        zona = prefs.getString(MainActivity.ZONA, null);
        mAdapter = new PlayaAdapter(getActivity(), playas);
        requestQueue = SingletonVolley.getInstance(getContext()).getRequestQueue();

        // Si se ejecutó alguna vez el programa
        if (zona != null) {

            // Configurar la lista
            lvItems = (ListView) rootView.findViewById(R.id.list_view_playas);

                if (savedInstanceState != null) {
                    playas = savedInstanceState.getParcelableArrayList(bPlayas);
                    mAdapter = new PlayaAdapter(getActivity(), playas);
                    lvItems.setAdapter(mAdapter);
                    lvItems.setOnItemClickListener(this);
                } else {

                    // Si alguna vez se guardó en un fichero las playas de una zona,
                    // se guardó también la fecha en la que se hizo
                    String fecha = null;
                    if(zona.equals("Oeste")){
                        fecha = prefs.getString(FECHA_OESTE, null);
                    }
                    else if(zona.equals("Centro")){
                        fecha = prefs.getString(FECHA_CENTRO, null);
                    }
                    else if(zona.equals("Este")){
                        fecha = prefs.getString(FECHA_ESTE, null);
                    }

                    Date date = null;
                    if(fecha != null){
                        SimpleDateFormat sdf = new SimpleDateFormat("dd/M/yyyy HH:mm");
                        try {
                            date = sdf.parse(fecha);
                            // Si se guardó el fichero hace más de una hora se tienen que actualizar las predicciones
                            date.setHours(date.getHours() + 1);
                        } catch(ParseException e){

                        }
                    }

                    Date fechaActual = new Date();

                    boolean restore = !restoreList();
                    if(restore || date == null || date.compareTo(fechaActual) < 0) {


                        textViewActualizando = (TextView) rootView.findViewById(R.id.textViewActualizando);
                        textViewActualizando.setVisibility(View.VISIBLE);

                        if (zona.equals("Oeste")) {
                            nombres = getResources().getStringArray(R.array.playas_oeste);
                            localidades = getResources().getStringArray(R.array.localidades_oeste);
                            codigos = codigos_oeste;
                            coordenadas = coordenadas_oeste;
                        } else if (zona.equals("Centro")) {
                            nombres = getResources().getStringArray(R.array.playas_centro);
                            localidades = getResources().getStringArray(R.array.localidades_centro);
                            codigos = codigos_centro;
                            coordenadas = coordenadas_centro;
                        } else if (zona.equals("Este")) {
                            nombres = getResources().getStringArray(R.array.playas_este);
                            localidades = getResources().getStringArray(R.array.localidades_este);
                            codigos = codigos_este;
                            coordenadas = coordenadas_este;
                        }


                        mAdapter = new PlayaAdapter(getActivity(), createPlayaList(nombres, localidades));
                        lvItems.setAdapter(mAdapter);
                        lvItems.setOnItemClickListener(this);

                        // Hacer peticiones
                        String url = URL + String.valueOf(codigos[0] + apiKey);

                        // Si la predicción ha cambiado (después de guardar en un fichero los datos)
                        contador = 0;
                        textViewActualizando = (TextView) rootView.findViewById(R.id.textViewActualizando);
                        textViewActualizando.setVisibility(View.VISIBLE);
                        playas.clear();
                        parseConsultaPlaya(url);
                        //saveList();
                        return rootView;
                    }
                    else {
                        mAdapter = new PlayaAdapter(getActivity(), playas);
                        lvItems.setAdapter(mAdapter);
                        lvItems.setOnItemClickListener(this);

                    }
            }




        }return rootView;
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {

        outState.putParcelableArrayList(bPlayas, playas);
        super.onSaveInstanceState(outState);

    }

    private List<Playa> createPlayaList(String [] nombres, String [] localidades) {

        if(nombres.length != localidades.length){
            throw new IllegalStateException();
        }

        ArrayList<Playa> playas = new ArrayList<Playa>(nombres.length);
        for(int i = 0; i < nombres.length; i++){
            playas.add(new Playa(nombres[i], localidades[i], codigos[i], coordenadas[i], null));
        }

        return playas;

    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id){

        final Playa playa = (Playa) parent.getItemAtPosition(position);
        if(playa.getPrediccion() == null){
            consulta(URL + String.valueOf(playa.getCodigo()) + apiKey, new FilterFragment.Callback() {
                @Override
                public void onSuccess(Prevision p) {
                    playa.setPrediccion(p);
                    mCallback.onPlayaSelected(playa);
                    return;
                }
            });
        }
        else {
            mCallback.onPlayaSelected(playa);
        }

    }

    public void parseConsultaPlaya(final String url){

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET,
                url, null, new Response.Listener<JSONObject>() {

            @Override
            public void onResponse(JSONObject response) {
                try {
                    String urlPlaya = String.valueOf(response.getString("datos"));
                    JsonArrayRequest jsonArrayRequest = new JsonArrayRequest(Request.Method.GET,
                            urlPlaya, null, new Response.Listener<JSONArray>() {
                        @Override
                        public void onResponse(JSONArray response) {
                            try {
                                JSONObject datos = response.getJSONObject(0);
                                Gson gson = new Gson();
                                ConsultaPlaya consulta = gson.fromJson(datos.toString(), ConsultaPlaya.class);
                                //consultas.add(consulta);
                                System.out.println(consulta.getPrediccion());
                                Dia dia = consulta.getPrediccion().getDia().get(0);
                                Prevision prevision = new Prevision(dia.getEstadoCielo().getF1(),
                                        dia.getEstadoCielo().getDescripcion1(), dia.getEstadoCielo().getF2(),
                                        dia.getEstadoCielo().getDescripcion2(), dia.getViento().getDescripcion1(),
                                        dia.getViento().getDescripcion2(), dia.getOleaje().getDescripcion1(), dia.getOleaje().getDescripcion2(),
                                        dia.gettMaxima().getValor1(), dia.getsTermica().getDescripcion1(), dia.gettAgua().getValor1(),
                                        dia.getUvMax().getValor1());
                                Playa p = new Playa(nombres[contador], localidades[contador], codigos[contador], coordenadas[contador], prevision);
                                playas.add(p);

                                // Se siguen haciendo consultas
                                if(playas.size() < codigos.length){
                                    String url = URL + codigos[playas.size()] + apiKey;
                                    contador++;
                                    parseConsultaPlaya(url);
                                }
                                if(playas.size() == codigos.length){
                                    // Ordenar y mostrar lista
                                    actualizarLista();
                                    saveList();
                                }



                            } catch (JSONException e) {
                                e.printStackTrace();
                                AlertDialog.Builder ab = new AlertDialog.Builder(getContext());
                                ab.setTitle("Error");
                                ab.setMessage(R.string.problemaConexion);
                                ab.setPositiveButton("OK", new DialogInterface.OnClickListener(){
                                    public void onClick(DialogInterface dialog, int which){
                                        getActivity().finish();
                                    }
                                });
                                AlertDialog alerta = ab.create();
                                alerta.show();

                            }
                        }
                    }, new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            error.printStackTrace();
                            AlertDialog.Builder ab = new AlertDialog.Builder(getContext());
                            ab.setTitle("Error");
                            ab.setMessage(R.string.problemaConexion);
                            ab.setPositiveButton("OK", new DialogInterface.OnClickListener(){
                                public void onClick(DialogInterface dialog, int which){
                                    getActivity().finish();
                                }
                            });
                            AlertDialog alerta = ab.create();
                            alerta.show();
                        }
                    });

                    requestQueue.add(jsonArrayRequest);

                } catch (JSONException e) {
                    e.printStackTrace();
                    AlertDialog.Builder ab = new AlertDialog.Builder(getContext());
                    ab.setTitle("Error");
                    ab.setMessage(R.string.problemaConexion);
                    ab.setPositiveButton("OK", new DialogInterface.OnClickListener(){
                        public void onClick(DialogInterface dialog, int which){
                            getActivity().finish();
                        }
                    });
                    AlertDialog alerta = ab.create();
                    alerta.show();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
                AlertDialog.Builder ab = new AlertDialog.Builder(getContext());
                ab.setTitle("Error");
                ab.setMessage(R.string.problemaConexion);
                ab.setPositiveButton("OK", new DialogInterface.OnClickListener(){
                    public void onClick(DialogInterface dialog, int which){
                        getActivity().finish();
                    }
                });
                AlertDialog alerta = ab.create();
                alerta.show();
            }

        });
        requestQueue.add(jsonObjectRequest);

    }

    public void actualizarLista(){

        // Ordenar playas según temperatura y cielo
        for(int i = 0; i < playas.size()-1; i++){
            for(int j = i+1; j < playas.size(); j++){
                Date date = new Date();
                int cielo_i;
                int cielo_j;
                if(date.getHours() <= 14){
                    cielo_i = playas.get(i).getPrediccion().getCieloMananaCodigo();
                    cielo_j = playas.get(j).getPrediccion().getCieloMananaCodigo();
                }
                else{
                    cielo_i = playas.get(i).getPrediccion().getCieloTardeCodigo();
                    cielo_j = playas.get(j).getPrediccion().getCieloTardeCodigo();
                }

                int temperatura_i = playas.get(i).getPrediccion().getTemperatura();
                int temperatura_j = playas.get(j).getPrediccion().getTemperatura();

                System.out.println("i " + playas.get(i).getNombre() + ": " + cielo_i + " " + temperatura_i);
                System.out.println("j " + playas.get(j).getNombre() + ": " + cielo_j + " " + temperatura_j);

                //if(temperatura_i < temperatura_j && cielo_i >= cielo_j ){
                if(cielo_i > cielo_j || (cielo_i == cielo_j && temperatura_i < temperatura_j)){

                    System.out.println("SE CAMBIA");

                    Playa p = playas.get(i);
                    playas.remove(i);
                    if(i < playas.size()-1)
                        playas.add(i, playas.get(j-1));
                    else
                        playas.add(playas.get(j-1));
                    playas.remove(j);
                    if(j < playas.size()-1)
                        playas.add(j, p);
                    else
                        playas.add(p);
                }
            }
        }

        Arrays.fill(nombres, null);
        Arrays.fill(localidades, null);
        for(int i = 0; i < playas.size(); i++){
            nombres[i] = playas.get(i).getNombre();
            localidades[i] = playas.get(i).getLocalidad();

        }
        textViewActualizando.setVisibility(View.INVISIBLE);
        mAdapter = new PlayaAdapter(getActivity(), playas);
        lvItems.setAdapter(mAdapter);
        lvItems.setOnItemClickListener(this);

    }

    public void consulta(final String url, final FilterFragment.Callback onCallback){

        requestQueue = SingletonVolley.getInstance(getContext()).getRequestQueue();

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET,
                url, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                    String urlPlaya = String.valueOf(response.getString("datos"));
                    JsonArrayRequest jsonArrayRequest = new JsonArrayRequest(Request.Method.GET,
                            urlPlaya, null, new Response.Listener<JSONArray>() {
                        @Override
                        public void onResponse(JSONArray response) {
                            try {
                                JSONObject datos = response.getJSONObject(0);
                                Gson gson = new Gson();
                                ConsultaPlaya consulta = gson.fromJson(datos.toString(), ConsultaPlaya.class);
                                Dia dia = consulta.getPrediccion().getDia().get(0);
                                Prevision prevision = new Prevision(dia.getEstadoCielo().getF1(),
                                        dia.getEstadoCielo().getDescripcion1(), dia.getEstadoCielo().getF2(),
                                        dia.getEstadoCielo().getDescripcion2(), dia.getViento().getDescripcion1(),
                                        dia.getViento().getDescripcion2(), dia.getOleaje().getDescripcion1(), dia.getOleaje().getDescripcion2(),
                                        dia.gettMaxima().getValor1(), dia.getsTermica().getDescripcion1(), dia.gettAgua().getValor1(),
                                        dia.getUvMax().getValor1());
                                onCallback.onSuccess(prevision);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    }, new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            error.printStackTrace();
                            AlertDialog.Builder ab = new AlertDialog.Builder(getContext());
                            ab.setTitle("Error");
                            ab.setMessage(R.string.problemaConexion);
                            ab.setPositiveButton("OK", new DialogInterface.OnClickListener(){
                                public void onClick(DialogInterface dialog, int which){
                                    getActivity().finish();
                                }
                            });
                            AlertDialog alerta = ab.create();
                            alerta.show();
                        }
                    });
                    requestQueue.add(jsonArrayRequest);
                } catch (JSONException e) {
                    e.printStackTrace();

                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
            }
        });
        requestQueue.add(jsonObjectRequest);

    }


    void cambiarZona(){

        if(Locale.getDefault().getDisplayLanguage().equals("English")){
            final CharSequence[] aux = {"West", "Centre", "East"};
            items = aux;
        }
        else{
            final CharSequence[] aux = {"Oeste", "Centro", "Este"};
            items = aux;
        }



        actual = 0;
        if(zona.equals("Oeste")){
            actual = 0;
        }
        else if(zona.equals("Centro")){
            actual = 1;
        }
        else if(zona.equals("Este")){
            actual = 2;
        }

        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        builder.setTitle(R.string.seleccionaZonaDialogo);
        builder.setSingleChoiceItems(items, actual, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                elementoSeleccionado = which;
            }
        }).setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Toast.makeText(getContext(), "OK", Toast.LENGTH_SHORT);
                if(elementoSeleccionado != actual) {
                    zona = items[elementoSeleccionado].toString();
                    if(Locale.getDefault().getDisplayLanguage().equals("English")){
                        switch(zona){
                            case "East":
                                zona = "Este";
                                break;
                            case "Centre":
                                zona = "Centro";
                                break;
                            case "West":
                                zona = "Oeste";
                                break;
                        }
                    }
                    prefs = getActivity().getSharedPreferences(PrimeraEjecucionFragment.PREFERENCES, Context.MODE_PRIVATE);
                    final SharedPreferences.Editor prefsEditor = prefs.edit();
                    prefsEditor.putString(PrimeraEjecucionFragment.ZONA, zona);
                    prefsEditor.commit();
                    // Si alguna vez se guardó en un fichero las playas de una zona,
                    // se guardó también la fecha en la que se hizo
                    String fecha = null;
                    if(zona.equals("Oeste")){
                        fecha = prefs.getString(FECHA_OESTE, null);
                    }
                    else if(zona.equals("Centro")){
                        fecha = prefs.getString(FECHA_CENTRO, null);
                    }
                    else if(zona.equals("Este")){
                        fecha = prefs.getString(FECHA_ESTE, null);
                    }

                    Date date = null;
                    Date fechaActual = new Date();
                    int comparaFechas = 0;
                    if(fecha != null){
                        SimpleDateFormat sdf = new SimpleDateFormat("dd/M/yyyy HH:mm");
                        try {
                            date = sdf.parse(fecha);
                            // Si se guardó el fichero hace más de una hora se tienen que actualziar las predicciones
                            date.setHours(date.getHours() + 1);
                            comparaFechas = date.compareTo(fechaActual);
                        } catch(ParseException e){

                        }
                    }



                    boolean restore = !restoreList();
                    if(restore || date == null || comparaFechas < 0) {
                        if (zona.equals("Oeste")) {
                            nombres = getResources().getStringArray(R.array.playas_oeste);
                            localidades = getResources().getStringArray(R.array.localidades_oeste);
                            codigos = codigos_oeste;
                            coordenadas = coordenadas_oeste;

                        } else if (zona.equals("Centro")) {
                            nombres = getResources().getStringArray(R.array.playas_centro);
                            localidades = getResources().getStringArray(R.array.localidades_centro);
                            codigos = codigos_centro;
                            coordenadas = coordenadas_centro;
                        } else if (zona.equals("Este")) {
                            nombres = getResources().getStringArray(R.array.playas_este);
                            localidades = getResources().getStringArray(R.array.localidades_este);
                            codigos = codigos_este;
                            coordenadas = coordenadas_este;
                        }


                        mAdapter = new PlayaAdapter(getActivity(), createPlayaList(nombres, localidades));
                        lvItems.setAdapter(mAdapter);
                        String url = URL + String.valueOf(codigos[0] + apiKey);

                        // Si la predicción ha cambiado (después de guardar en un fichero los datos)
                        contador = 0;
                        textViewActualizando = (TextView) rootView.findViewById(R.id.textViewActualizando);
                        textViewActualizando.setVisibility(View.VISIBLE);
                        playas.clear();
                        parseConsultaPlaya(url);

                    }
                    else{

                        mAdapter = new PlayaAdapter(getActivity(), playas);
                        lvItems.setAdapter(mAdapter);
                    }
                }

            }
        }).setNegativeButton(R.string.cancelar, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        });
        AlertDialog dialog = builder.create();
        dialog.show();
    }

    private void saveList(){

        FileOutputStream file = null;
        OutputStream buffer = null;
        ObjectOutput output = null;

        SimpleDateFormat sdf = new SimpleDateFormat("dd/M/yyyy HH:mm");
        String date = sdf.format(new Date());

        try{
            //file = getActivity().openFileOutput(PLAYAS_LIST_FILENAME, Context.MODE_PRIVATE);
            if(zona.equals("Oeste")){
                file = getActivity().openFileOutput(PLAYAS_OESTE_LIST_FILENAME, Context.MODE_PRIVATE);

                prefsEditor.putString(FECHA_OESTE, date);
                prefsEditor.commit();
            }
            else if(zona.equals("Centro")){
                file = getActivity().openFileOutput(PLAYAS_CENTRO_LIST_FILENAME, Context.MODE_PRIVATE);

                prefsEditor.putString(FECHA_CENTRO, date);
                prefsEditor.commit();

            }
            else if(zona.equals("Este")){
                file = getActivity().openFileOutput(PLAYAS_ESTE_LIST_FILENAME, Context.MODE_PRIVATE);

                prefsEditor.putString(FECHA_ESTE, date);
                prefsEditor.commit();

            }
            buffer = new BufferedOutputStream(file);
            output = new ObjectOutputStream(buffer);
            output.writeObject(playas);
        } catch (Exception e){
            e.printStackTrace();
        } finally{
            try{
                output.close();
            }catch (IOException e){

            }
        }

    }

    private boolean restoreList(){

        InputStream buffer = null;
        ObjectInput input = null;
        Playa p = null;

        try{
            //buffer = new BufferedInputStream(getActivity().openFileInput(PLAYAS_LIST_FILENAME));

            if(zona.equals("Oeste")){
                buffer = new BufferedInputStream(getActivity().openFileInput(PLAYAS_OESTE_LIST_FILENAME));
            }
            else if(zona.equals("Centro")){
                buffer = new BufferedInputStream(getActivity().openFileInput(PLAYAS_CENTRO_LIST_FILENAME));
            }
            else if(zona.equals("Este")){
                buffer = new BufferedInputStream(getActivity().openFileInput(PLAYAS_ESTE_LIST_FILENAME));
            }

            input = new ObjectInputStream(buffer);
            playas = (ArrayList<Playa>)input.readObject();

            if(zona.equals("Oeste")){
                if(playas.size() != 9)
                    return false;
            }
            else if(zona.equals("Centro")){
                if(playas.size() != 7){
                    return false;
                }
            }
            else if(zona.equals("Este")){
                if(playas.size() != 10){
                    return false;
                }
            }
            return true;
        } catch (Exception e){

        } finally {
            try{
                input.close();
            } catch (Exception e){

            }
        }
        return false;
    }



}
