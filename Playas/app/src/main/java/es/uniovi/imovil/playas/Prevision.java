package es.uniovi.imovil.playas;

import android.os.Parcel;
import android.os.Parcelable;

import java.io.Serializable;

/**
 * Created by cristina on 1/5/17.
 */

public class Prevision implements Parcelable, Serializable{

    int cieloMananaCodigo;
    String cieloManana;
    int cieloTardeCodigo;
    String cieloTarde;

    String vientoManana;
    String vientoTarde;

    String oleajeManana;
    String oleajeTarde;

    int temperatura;

    String sensacionTermica;

    int temperaturaAgua;

    int indiceUV;


    public int getCieloMananaCodigo() {
        return cieloMananaCodigo;
    }

    public void setCieloMananaCodigo(int cieloMananaCodigo){
        this.cieloMananaCodigo = cieloMananaCodigo;
    }

    public String getCieloManana() {
        return cieloManana;
    }

    public int getCieloTardeCodigo() {
        return cieloTardeCodigo;
    }

    public String getCieloTarde() {
        return cieloTarde;
    }

    public String getVientoManana() {
        return vientoManana;
    }

    public String getVientoTarde() {
        return vientoTarde;
    }

    public String getOleajeManana() {
        return oleajeManana;
    }

    public String getOleajeTarde() {
        return oleajeTarde;
    }

    public int getTemperatura() {
        return temperatura;
    }

    public String getSensacionTermica() {
        return sensacionTermica;
    }

    public int getTemperaturaAgua() {
        return temperaturaAgua;
    }

    public int getIndiceUV() {
        return indiceUV;
    }

    public Prevision(int cieloMananaCodigo, String cieloManana, int cieloTardeCodigo,
                     String cieloTarde, String vientoManana, String vientoTarde, String oleajeManana,
                     String oleajeTarde, int temperatura, String sensacionTermica, int temperaturaAgua,
                     int indiceUV) {
        this.cieloMananaCodigo = cieloMananaCodigo;
        this.cieloManana = cieloManana;
        this.cieloTardeCodigo = cieloTardeCodigo;
        this.cieloTarde = cieloTarde;
        this.vientoManana = vientoManana;
        this.vientoTarde = vientoTarde;
        this.oleajeManana = oleajeManana;
        this.oleajeTarde = oleajeTarde;
        this.temperatura = temperatura;
        this.sensacionTermica = sensacionTermica;
        this.temperaturaAgua = temperaturaAgua;
        this.indiceUV = indiceUV;
    }

    public Prevision(Parcel parcel){

        readFromParcel(parcel);

    }

    @Override
    public int describeContents(){

        return 0;

    }

    @Override
    public void writeToParcel(Parcel dest, int flags){

        dest.writeInt(cieloMananaCodigo);
        dest.writeString(cieloManana);
        dest.writeInt(cieloTardeCodigo);
        dest.writeString(cieloTarde);
        dest.writeString(vientoManana);
        dest.writeString(vientoTarde);
        dest.writeString(oleajeManana);
        dest.writeString(oleajeTarde);
        dest.writeInt(temperatura);
        dest.writeString(sensacionTermica);
        dest.writeInt(temperaturaAgua);
        dest.writeInt(indiceUV);

    }

    public void readFromParcel(Parcel parcel){

        cieloMananaCodigo = parcel.readInt();
        cieloManana = parcel.readString();
        cieloTardeCodigo = parcel.readInt();
        cieloTarde = parcel.readString();
        vientoManana = parcel.readString();
        vientoTarde = parcel.readString();
        oleajeManana = parcel.readString();
        oleajeTarde = parcel.readString();
        temperatura = parcel.readInt();
        sensacionTermica = parcel.readString();
        temperaturaAgua = parcel.readInt();
        indiceUV = parcel.readInt();

    }

    public static final Parcelable.Creator<Prevision> CREATOR =
            new Parcelable.Creator<Prevision>(){

                public Prevision createFromParcel(Parcel in){

                    return new Prevision(in);

                }

                public Prevision[] newArray(int size){

                    return new Prevision[size];

                }

            };
}
