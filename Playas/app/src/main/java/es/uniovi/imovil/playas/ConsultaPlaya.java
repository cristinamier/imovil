package es.uniovi.imovil.playas;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by cristina on 29/4/17.
 */

public class ConsultaPlaya {

    Origen origen;
    String elaborado;
    String nombre;
    String localidad;
    Prediccion prediccion;
    long id;

    public Origen getOrigen(){
        return origen;
    }

    public String getElaborado() {
        return elaborado;
    }

    public String getNombre() {
        return nombre;
    }

    public String getLocalidad() {
        return localidad;
    }

    public Prediccion getPrediccion() {
        return prediccion;
    }

    public long getId() {
        return id;
    }

    public static class Origen {

        String productor;
        String web;
        String language;
        String copyright;
        String notaLegal;

        public String getProductor() {
            return productor;
        }

        public String getWeb() {
            return web;
        }

        public String getLanguage() {
            return language;
        }

        public String getCopyright() {
            return copyright;
        }

        public String getNotaLegal() {
            return notaLegal;
        }

    }

    public static class Prediccion implements Parcelable{

        ArrayList<Dia> dia;

        public Prediccion(){
            dia = new ArrayList<Dia>();
        }

        public ArrayList<Dia> getDia() {
            return dia;
        }

        public Prediccion(Parcel parcel){
            this();
            readFromParcel(parcel);

        }

        @Override
        public int describeContents(){

            return 0;

        }

        @Override
        public void writeToParcel(Parcel dest, int flags){

            dest.writeList(dia);

        }

        public void readFromParcel(Parcel parcel){

            //parcel.readList(dia, Dia.class.getClassLoader());
            parcel.readTypedList(dia, Dia.CREATOR);

        }

        public static final Parcelable.Creator<Prediccion> CREATOR =
                new Parcelable.Creator<Prediccion>(){

                    public Prediccion createFromParcel(Parcel in){

                        return new Prediccion(in);

                    }

                    public Prediccion[] newArray(int size){

                        return new Prediccion[size];

                    }

                };

    }

}
