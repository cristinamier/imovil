package es.uniovi.imovil.playas;

import android.content.Intent;
import android.os.Parcelable;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;

import java.util.ArrayList;

public class ResultadosFiltroActivity extends AppCompatActivity implements ResultadosFiltroFragment.Callback{

    public static final String PLAYAS = "es.uniovi.imovil.user.playas.PLAYAS";
    ResultadosFiltroFragment mFragment;
    private boolean mTwoPanes = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.resultados);

        Intent intent = getIntent();
        ArrayList<Playa> playas = intent.getParcelableArrayListExtra(PLAYAS);
        Log.i("ResultadosFiltroActivit", "Longitud de playas: " + playas.size());

        if(findViewById(R.id.resultados_details_container) != null){
            mTwoPanes = true;
        }

        // ¿Existe el contenedor del fragmento?
            Log.i("ResultadosFiltroActivit", "Fragmento not null");

            // Si estamos restaurando desde un estado previo no hacemos nada
            if(savedInstanceState != null){
                return;
            }
            getSupportActionBar().setTitle(R.string.resultados);


            if(mTwoPanes){
                FragmentManager fragmentManager = getSupportFragmentManager();
                ResultadosFiltroFragment fragment = (ResultadosFiltroFragment) fragmentManager.findFragmentById(R.id.resultados_frag);
                fragment.listarPlayas(playas);
            }
            else{
                ResultadosFiltroFragment fragment = ResultadosFiltroFragment.newInstance(playas);
                getSupportFragmentManager().beginTransaction().add(R.id.fragment_resultados_container, fragment).commit();
            }



            getSupportActionBar().setDisplayHomeAsUpEnabled(true);

}

    @Override
    public void onPlayaSelected(Playa playa){
        if(mTwoPanes){
            FragmentManager fragmentManager = getSupportFragmentManager();
            PlayaDetailsFragment fragment = (PlayaDetailsFragment) fragmentManager.findFragmentById(R.id.resultados_details_frag);
            fragment.setDescription(playa);
            fragment.addMarker(playa.getNombre(), playa.getCoordenadas());
        }
        else {
            Intent intent = new Intent(this, PlayaDetailsActivity.class);
            intent.putExtra(PlayaDetailsActivity.PLAYA, (Parcelable)playa);
            startActivity(intent);
        }
    }

}
