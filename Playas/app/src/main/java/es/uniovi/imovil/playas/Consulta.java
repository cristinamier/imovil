package es.uniovi.imovil.playas;

import android.content.Context;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by cristina on 7/5/17.
 */

public class Consulta {

    RequestQueue requestQueue;

    public void consulta(final String url, Context contexto, final FilterFragment.Callback onCallback){

        requestQueue = SingletonVolley.getInstance(contexto).getRequestQueue();

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET,
                url, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                    String urlPlaya = String.valueOf(response.getString("datos"));
                    JsonArrayRequest jsonArrayRequest = new JsonArrayRequest(Request.Method.GET,
                            urlPlaya, null, new Response.Listener<JSONArray>() {
                        @Override
                        public void onResponse(JSONArray response) {
                            try {
                                JSONObject datos = response.getJSONObject(0);
                                Gson gson = new Gson();
                                ConsultaPlaya consulta = gson.fromJson(datos.toString(), ConsultaPlaya.class);
                                Dia dia = consulta.getPrediccion().getDia().get(0);
                                Prevision prevision = new Prevision(dia.getEstadoCielo().getF1(),
                                        dia.getEstadoCielo().getDescripcion1(), dia.getEstadoCielo().getF2(),
                                        dia.getEstadoCielo().getDescripcion2(), dia.getViento().getDescripcion1(),
                                        dia.getViento().getDescripcion2(), dia.getOleaje().getDescripcion1(), dia.getOleaje().getDescripcion2(),
                                        dia.gettMaxima().getValor1(), dia.getsTermica().getDescripcion1(), dia.gettAgua().getValor1(),
                                        dia.getUvMax().getValor1());
                                onCallback.onSuccess(prevision);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    }, new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            error.printStackTrace();
                        }
                    });
                    requestQueue.add(jsonArrayRequest);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
            }
        });
        requestQueue.add(jsonObjectRequest);
    }

}
