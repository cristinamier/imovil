package es.uniovi.imovil.playas;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationServices;

import java.util.ArrayList;

public class FilterActivity extends ActionBarActivity implements FilterFragment.CallbackLocalidad, GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener{

    GoogleApiClient mGoogleApiclient;
    Location localizacionActual;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.filter_activity);

        if(mGoogleApiclient == null){
            mGoogleApiclient = new GoogleApiClient.Builder(this)
                    .addConnectionCallbacks(this).addOnConnectionFailedListener(this)
                    .addApi(LocationServices.API).build();

        }
        if(mGoogleApiclient !=  null){
            mGoogleApiclient.connect();
        }

        getSupportActionBar().setTitle(R.string.filtro);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);


    }

    @Override
    public void onConnected(@Nullable Bundle bundle){

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            //Toast.makeText(this, "Faltan permisos", Toast.LENGTH_SHORT).show();
        }else{
            localizacionActual = LocationServices.FusedLocationApi.getLastLocation(
                    mGoogleApiclient);


            /*if(localizacionActual != null){
                Log.i("COORDENADAS", String.valueOf(localizacionActual.getLatitude()) +
                        String.valueOf(localizacionActual.getLongitude()));
            }
            else{
                Log.i("COORDENADAS", "null");
            }*/

        }
        if(findViewById(R.id.fragment_filter_container) != null){

            // Crear el fragmento
            FilterFragment fragment = FilterFragment.newInstance(localizacionActual);
            // Añadir el fragmento contenedor 'fragment_filter_container'
            getSupportFragmentManager().beginTransaction().add(R.id.fragment_filter_container, fragment).commit();

        }
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onSuccess(ArrayList<Playa> playas){
        Intent intent = new Intent(this, ResultadosFiltroActivity.class);
        intent.putExtra(ResultadosFiltroActivity.PLAYAS, playas);
        startActivity(intent);
    }

}
