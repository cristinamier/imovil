package es.uniovi.imovil.playas;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

/**
 * Created by cristina on 29/4/17.
 */

public class PrimeraEjecucionActivity extends AppCompatActivity{

    @Override
    protected void onCreate(Bundle saveInstanceState){

        super.onCreate(saveInstanceState);
        setContentView(R.layout.primera_ejecucion_activity);

        if(findViewById(R.id.fragment_primera_ejecucion_container) != null){

            // Crear el fragmento
            PrimeraEjecucionFragment fragment = PrimeraEjecucionFragment.newInstance();

            // Añadir el fragmento al ontenedor 'fragment_primera_ejecucion_container'
            getSupportFragmentManager().beginTransaction().add(R.id.fragment_primera_ejecucion_container, fragment).commit();


        }

    }

}
