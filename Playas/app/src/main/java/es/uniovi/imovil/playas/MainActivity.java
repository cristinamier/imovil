package es.uniovi.imovil.playas;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.os.Parcelable;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.NotificationCompat;
import android.support.v4.view.MenuItemCompat;
import android.support.v4.widget.CursorAdapter;
import android.support.v4.widget.SimpleCursorAdapter;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.SearchView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;

import java.util.ArrayList;

public class MainActivity extends ActionBarActivity implements PlayaListFragment.Callbacks/*, FilterFragment.CallbackLocalidad*/{

    private boolean mTwoPanes = false;
    public static final String PREFERENCES = "preferences";
    public static final String ZONA = "zona";
    private SharedPreferences prefs = null;
    public String zona = null;
    public static final int CONNECTION_FAILURE_RESOLUTION_REQUEST = 2;
    public static final int RESULT_OK = 0;
    //public static final String PLAYAS = "es.uniovi.imovil.cristina.playa.PLAYAS";

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);

        if(isGooglePlayServicesAvailable() == false){
            finish();
        }

        if(findViewById(R.id.playa_details_container) != null){
            mTwoPanes = true;
        }

        if(savedInstanceState != null){
            return;
        }

        SharedPreferences prefs = getSharedPreferences(PREFERENCES, Context.MODE_PRIVATE);
        zona = prefs.getString(ZONA, null);

        // Si la aplicación nunca se ejecutó
        if(zona == null){
            Intent intent2 = new Intent(this, PrimeraEjecucionActivity.class);
            startActivity(intent2);
            finish();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        // Inflar el menú y añadir acciones al action bar si existe
        getMenuInflater().inflate(R.menu.main, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item){

        switch (item.getItemId()){
            case R.id.action_filter:
                Intent intent = new Intent(this, FilterActivity.class);
                startActivity(intent);
                return true;
            case R.id.action_settings:
                FragmentManager fragmentManager = getSupportFragmentManager();
                PlayaListFragment fragment = (PlayaListFragment) fragmentManager.findFragmentById(R.id.playa_list_frag);
                fragment.cambiarZona();
            default:
                return super.onOptionsItemSelected(item);
        }

    }

    @Override
    public void onPlayaSelected(Playa playa){

        if(mTwoPanes) {
            FragmentManager fragmentManager = getSupportFragmentManager();
            PlayaDetailsFragment fragment = (PlayaDetailsFragment) fragmentManager.findFragmentById(R.id.playa_details_frag);
            fragment.setDescription(playa);
            fragment.addMarker(playa.getNombre(), playa.getCoordenadas());
        } else{
            Intent intent = new Intent(this, PlayaDetailsActivity.class);
            intent.putExtra(PlayaDetailsActivity.PLAYA, (Parcelable) playa);
            startActivity(intent);
        }

    }

    private boolean isGooglePlayServicesAvailable() {
        int result = GooglePlayServicesUtil.isGooglePlayServicesAvailable(this);
        if (result == ConnectionResult.SUCCESS) {
            // El dispositivo tiene los servicios disponibles
            return true;
        }

        // Hay algún problema. Comprobar si lo puede resolver el usuario
        if (!GooglePlayServicesUtil.isUserRecoverableError(result)) {
            // No lo puede resolver el usuario. Mostrar un mensaje
            Toast.makeText(
                    this,
                    R.string.requiereGoogleServices,
                    Toast.LENGTH_LONG).show();
            return false;
        }

        // Sí lo puede resolver el usuario. Mostrar el diálogo
        Dialog errorDialog = GooglePlayServicesUtil.getErrorDialog(result,
                this, CONNECTION_FAILURE_RESOLUTION_REQUEST);
        if (errorDialog != null) {
            errorDialog.show();
        } else {
            Toast.makeText(
                    this,
                    R.string.requiereGoogleServices,
                    Toast.LENGTH_LONG).show();
        }
        return false; // De momento, no están disponibles
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == CONNECTION_FAILURE_RESOLUTION_REQUEST) {
            // El usuario ha acabado con la gestión del problema en los
            // Google Play Services
            if (resultCode == RESULT_OK) {
                // Lo ha arreglado. No hace falta que hagamos nada: se hará
                // en el onResume()
            } else {
                // No lo ha arreglado
                Toast.makeText(
                        this,
                        R.string.noArregladoProblemaError + " "
                                + resultCode + ". "
                                + R.string.sinGoogleNoUtilizar,
                        Toast.LENGTH_LONG).show();
            }
        } else {
            super.onActivityResult(requestCode, resultCode, data);
        }
    }
}
