package es.uniovi.imovil.playas;

import android.app.admin.SystemUpdatePolicy;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.Locale;

/**
 * Created by cristina on 5/4/17.
 */

public class PlayaDetailsFragment extends Fragment{

    private static final String PLAYA_ARG = "playa";
    TextView tvCieloManana;
    TextView tvCieloTarde;
    TextView tvVientoManana;
    TextView tvVientoTarde;
    TextView tvOleajeManana;
    TextView tvOleajeTarde;
    TextView tvTemperatura;
    TextView tvSensacion;
    TextView tvTemperaturaAgua;
    TextView tvIndiceUV;
    private GoogleMap mMap;
    public static final String bPlaya = "playa";
    Playa playa = null;
    View rootView;

    public static PlayaDetailsFragment newInstance(Playa playa) {

        PlayaDetailsFragment fragment = new PlayaDetailsFragment();

        Bundle args = new Bundle();
        args.putParcelable(PLAYA_ARG, playa);
        fragment.setArguments(args);

        return fragment;

    }

    public PlayaDetailsFragment(){

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState){


        rootView = inflater.inflate(R.layout.playa_details_fragment, container, false);

        Bundle args = getArguments();
        /*tvCieloManana = (TextView) rootView.findViewById(R.id.textViewCieloM);
        tvCieloTarde = (TextView) rootView.findViewById(R.id.textViewCieloT);
        tvVientoManana = (TextView) rootView.findViewById(R.id.textViewVientoM);
        tvVientoTarde = (TextView) rootView.findViewById(R.id.textViewVientoT);
        tvOleajeManana = (TextView) rootView.findViewById(R.id.textViewOleajeM);
        tvOleajeTarde = (TextView) rootView.findViewById(R.id.textViewOleajeT);
        tvTemperatura = (TextView) rootView.findViewById(R.id.textViewTemperatura);
        tvSensacion = (TextView) rootView.findViewById(R.id.textViewSensacion);
        tvTemperaturaAgua = (TextView) rootView.findViewById(R.id.textViewTemperaturaAgua);
        tvIndiceUV = (TextView) rootView.findViewById(R.id.textViewIndiceUV);*/
        setUpMapIfNeeded();

        LatLng latLong = new LatLng(43.356929, -5.835940);
        CameraUpdate zoom = CameraUpdateFactory.zoomTo(3);
        CameraUpdate centro = CameraUpdateFactory.newLatLng(latLong);
        mMap.moveCamera(centro);
        mMap.animateCamera(zoom);

        if(savedInstanceState != null){
            playa = savedInstanceState.getParcelable(bPlaya);
            if(playa != null){
                setDescription(playa);
            }
            return rootView;
        }

        if(args != null){
            playa = args.getParcelable(PLAYA_ARG);
            setDescription(playa);
        }

        return rootView;

    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        if(playa != null) {
            outState.putParcelable(bPlaya, playa);
        }
        super.onSaveInstanceState(outState);
    }

    public void setDescription(Playa playa){
        this.playa = playa;
        System.out.println(Locale.getDefault().getDisplayLanguage());
        tvCieloManana = (TextView) rootView.findViewById(R.id.textViewCieloM);
        tvCieloTarde = (TextView) rootView.findViewById(R.id.textViewCieloT);
        tvVientoManana = (TextView) rootView.findViewById(R.id.textViewVientoM);
        tvVientoTarde = (TextView) rootView.findViewById(R.id.textViewVientoT);
        tvOleajeManana = (TextView) rootView.findViewById(R.id.textViewOleajeM);
        tvOleajeTarde = (TextView) rootView.findViewById(R.id.textViewOleajeT);
        tvTemperatura = (TextView) rootView.findViewById(R.id.textViewTemperatura);
        tvSensacion = (TextView) rootView.findViewById(R.id.textViewSensacion);
        tvTemperaturaAgua = (TextView) rootView.findViewById(R.id.textViewTemperaturaAgua);
        tvIndiceUV = (TextView) rootView.findViewById(R.id.textViewIndiceUV);


        String cieloManana = playa.getPrediccion().getCieloManana();
        String cieloTarde = playa.getPrediccion().getCieloTarde();
        String vientoManana = playa.getPrediccion().getVientoManana();
        String vientoTarde = playa.getPrediccion().getVientoTarde();
        String oleajeManana = playa.getPrediccion().getOleajeManana();
        String oleajeTarde = playa.getPrediccion().getOleajeTarde();
        String sensacionTermica = playa.getPrediccion().getSensacionTermica();
        if(Locale.getDefault().getDisplayLanguage().equals("English")){
            switch(cieloManana){
                case "despejado":
                    cieloManana = "clear";
                    break;
                case "nuboso":
                    cieloManana = "cloudy";
                    break;
                case "muy nuboso":
                    cieloManana = "very cloudy";
                    break;
                default:
                    cieloManana = "rainy";
            }

            switch(cieloTarde){
                case "despejado":
                    cieloTarde = "clear";
                    break;
                case "nuboso":
                    cieloTarde = "cloudy";
                    break;
                case "muy nuboso":
                    cieloTarde = "very cloudy";
                    break;
                default:
                    cieloTarde = "rainy";
            }

            switch(vientoManana){
                case "flojo":
                    vientoManana = "loose";
                    break;
                case "moderado":
                    vientoManana = "moderate";
                    break;
                case "fuerte":
                    vientoManana = "strong";
                    break;
            }

            switch(vientoTarde){
                case "flojo":
                    vientoTarde = "loose";
                    break;
                case "moderado":
                    vientoTarde = "moderate";
                    break;
                case "fuerte":
                    vientoTarde = "strong";
                    break;
            }

            switch(oleajeManana){
                case "débil":
                    oleajeManana = "weak";
                    break;
                case "moderado":
                    oleajeManana = "moderate";
                    break;
                case "fuerte":
                    oleajeManana = "strong";
                    break;
            }

            switch(oleajeTarde){
                case "débil":
                    oleajeTarde = "weak";
                    break;
                case "moderado":
                    oleajeTarde = "moderate";
                    break;
                case "fuerte":
                    oleajeTarde = "strong";
                    break;
            }

            switch(sensacionTermica){
                case "muy frío":
                    sensacionTermica = "very cold";
                    break;
                case "frío":
                    sensacionTermica = "cold";
                    break;
                case "muy fresco":
                    sensacionTermica = "very cool";
                    break;
                case "fresco":
                    sensacionTermica = "cool";
                    break;
                case "suave":
                    sensacionTermica = "soft";
                    break;
                default:
                    sensacionTermica = "hot";
            }
        }


        tvCieloManana.setText(cieloManana);
        tvCieloTarde.setText(cieloTarde);
        tvVientoManana.setText(vientoManana);
        tvVientoTarde.setText(vientoTarde);
        tvOleajeManana.setText(oleajeManana);
        tvOleajeTarde.setText(oleajeTarde);
        tvTemperatura.setText(String.valueOf(playa.getPrediccion().getTemperatura()));
        tvSensacion.setText(sensacionTermica);
        tvTemperaturaAgua.setText(String.valueOf(playa.getPrediccion().getTemperaturaAgua()));
        tvIndiceUV.setText(String.valueOf(playa.getPrediccion().getIndiceUV()));

        addMarker(playa.getNombre(), playa.getCoordenadas());

    }

    private void setUpMapIfNeeded() {
        // Comparamos con null para no instanciar el mapa más de una vez
        if (mMap != null) {
            return;
        }
        mMap = ((SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.map)).getMap();
        // Check if we were successful in obtaining the map.
        if (mMap == null) {
            // El mapa no ha sido cargado correctamente
            Toast.makeText(getContext(), R.string.noCargarMapa, Toast.LENGTH_LONG).show();
        }
    }

    void addMarker(String nombre, Coordenadas coordenadas) {
        double latitud = coordenadas.getLatitud();
        double longitud = coordenadas.getLongitud();
        LatLng latLong = new LatLng(latitud, longitud);

        MarkerOptions marker = new MarkerOptions().
                position(latLong)
                .title(nombre);

        mMap.clear();
        mMap.addMarker(marker);

        // Centrar mapa
        CameraUpdate centro = CameraUpdateFactory.newLatLng(latLong);
        CameraUpdate zoom = CameraUpdateFactory.zoomTo(12);

        mMap.moveCamera(centro);
        mMap.animateCamera(zoom);
    }


}
