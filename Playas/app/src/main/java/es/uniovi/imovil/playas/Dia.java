package es.uniovi.imovil.playas;

import android.os.Parcel;
import android.os.Parcelable;
import android.view.View;

/**
 * Created by cristina on 29/4/17.
 */

public class Dia implements Parcelable{

    EstadoCielo estadoCielo;
    Viento viento;
    Oleaje oleaje;
    TMaxima tMaxima;
    STermica sTermica;
    TAgua tAgua;
    UvMax uvMax;
    String fecha;

    public EstadoCielo getEstadoCielo() {
        return estadoCielo;
    }

    public Viento getViento() {
        return viento;
    }

    public Oleaje getOleaje() {
        return oleaje;
    }

    public TMaxima gettMaxima() {
        return tMaxima;
    }

    public STermica getsTermica() {
        return sTermica;
    }

    public TAgua gettAgua() {
        return tAgua;
    }

    public UvMax getUvMax() {
        return uvMax;
    }

    public String getFecha() {
        return fecha;
    }

    public Dia(Parcel parcel){

        readFromParcel(parcel);

    }

    @Override
    public int describeContents(){

        return 0;

    }

    @Override
    public void writeToParcel(Parcel dest, int flags){

        dest.writeParcelable(estadoCielo, flags);
        dest.writeParcelable(viento, flags);
        dest.writeParcelable(oleaje, flags);
        dest.writeParcelable(tMaxima, flags);
        dest.writeParcelable(sTermica, flags);
        dest.writeParcelable(tAgua, flags);
        dest.writeParcelable(uvMax, flags);
        dest.writeString(fecha);

    }

    public void readFromParcel(Parcel parcel){

        estadoCielo = (EstadoCielo) parcel.readParcelable(EstadoCielo.class.getClassLoader());
        viento = parcel.readParcelable(Viento.class.getClassLoader());
        oleaje = parcel.readParcelable(Oleaje.class.getClassLoader());
        tMaxima = parcel.readParcelable(TMaxima.class.getClassLoader());
        sTermica = parcel.readParcelable(STermica.class.getClassLoader());
        tAgua = parcel.readParcelable(TAgua.class.getClassLoader());
        uvMax = parcel.readParcelable(UvMax.class.getClassLoader());
        fecha = parcel.readString();

    }

    public static final Parcelable.Creator<Dia> CREATOR =
            new Parcelable.Creator<Dia>(){

                public Dia createFromParcel(Parcel in){

                    return new Dia(in);

                }

                public Dia[] newArray(int size){

                    return new Dia[size];

                }

            };

    public static class EstadoCielo implements Parcelable{

        String value;
        int f1;
        String descripcion1;
        int f2;
        String descripcion2;

        public String getValue() {
            return value;
        }

        public int getF1() {
            return f1;
        }

        public void setF1(int f1){
            this.f1 = f1;
        }

        public String getDescripcion1() {
            return descripcion1;
        }

        public int getF2() {
            return f2;
        }

        public void setF2(int f2){
            this.f2 = f2;
        }

        public String getDescripcion2() {
            return descripcion2;
        }


        public EstadoCielo(Parcel parcel){

            value = parcel.readString();
            f1 = parcel.readInt();
            descripcion1 = parcel.readString();
            f2 = parcel.readInt();
            descripcion2 = parcel.readString();
            readFromParcel(parcel);

        }

        @Override
        public int describeContents(){

            return 0;

        }

        @Override
        public void writeToParcel(Parcel dest, int flags){

            dest.writeString(value);
            dest.writeInt(f1);
            dest.writeString(descripcion1);
            dest.writeInt(f2);
            dest.writeString(descripcion2);

        }

        public void readFromParcel(Parcel parcel){

            value = parcel.readString();
            f1 = parcel.readInt();
            descripcion1 = parcel.readString();
            f2 = parcel.readInt();
            descripcion2 = parcel.readString();

        }

        public static final Parcelable.Creator<EstadoCielo> CREATOR =
                new Parcelable.Creator<EstadoCielo>(){

                    public EstadoCielo createFromParcel(Parcel in){

                        return new EstadoCielo(in);

                    }

                    public EstadoCielo[] newArray(int size){

                        return new EstadoCielo[size];

                    }

                };

    }

    public static class Viento implements Parcelable{

        String value;
        int f1;
        String descripcion1;
        int f2;
        String descripcion2;

        public String getValue() {
            return value;
        }

        public int getF1() {
            return f1;
        }

        public String getDescripcion1() {
            return descripcion1;
        }

        public int getF2() {
            return f2;
        }

        public String getDescripcion2() {
            return descripcion2;
        }

        public Viento(Parcel parcel){

            readFromParcel(parcel);

        }

        @Override
        public int describeContents(){

            return 0;

        }

        @Override
        public void writeToParcel(Parcel dest, int flags){

            dest.writeString(value);
            dest.writeInt(f1);
            dest.writeString(descripcion1);
            dest.writeInt(f2);
            dest.writeString(descripcion2);

        }

        public void readFromParcel(Parcel parcel){

            value = parcel.readString();
            f1 = parcel.readInt();
            descripcion1 = parcel.readString();
            f2 = parcel.readInt();
            descripcion2 = parcel.readString();

        }

        public static final Parcelable.Creator<Viento> CREATOR =
                new Parcelable.Creator<Viento>(){

                    public Viento createFromParcel(Parcel in){

                        return new Viento(in);

                    }

                    public Viento[] newArray(int size){

                        return new Viento[size];

                    }

                };

    }

    public static class Oleaje implements Parcelable{

        String value;
        int f1;
        String descripcion1;
        int f2;
        String descripcion2;

        public String getValue() {
            return value;
        }

        public int getF1() {
            return f1;
        }

        public String getDescripcion1() {
            return descripcion1;
        }

        public int getF2() {
            return f2;
        }

        public String getDescripcion2() {
            return descripcion2;
        }

        public Oleaje(Parcel parcel){

            readFromParcel(parcel);

        }

        @Override
        public int describeContents(){

            return 0;

        }

        @Override
        public void writeToParcel(Parcel dest, int flags){

            dest.writeString(value);
            dest.writeInt(f1);
            dest.writeString(descripcion1);
            dest.writeInt(f2);
            dest.writeString(descripcion2);

        }

        public void readFromParcel(Parcel parcel){

            value = parcel.readString();
            f1 = parcel.readInt();
            descripcion1 = parcel.readString();
            f2 = parcel.readInt();
            descripcion2 = parcel.readString();

        }

        public static final Parcelable.Creator<Oleaje> CREATOR =
                new Parcelable.Creator<Oleaje>(){

                    public Oleaje createFromParcel(Parcel in){

                        return new Oleaje(in);

                    }

                    public Oleaje[] newArray(int size){

                        return new Oleaje[size];

                    }

                };

    }

    public static class TMaxima implements Parcelable{

        String value;
        int valor1;

        public String getValue() {
            return value;
        }

        public int getValor1() {
            return valor1;
        }

        public TMaxima(Parcel parcel){

            readFromParcel(parcel);

        }

        @Override
        public int describeContents(){

            return 0;

        }

        @Override
        public void writeToParcel(Parcel dest, int flags){

            dest.writeString(value);
            dest.writeInt(valor1);

        }

        public void readFromParcel(Parcel parcel){

            value = parcel.readString();
            valor1 = parcel.readInt();

        }

        public static final Parcelable.Creator<TMaxima> CREATOR =
                new Parcelable.Creator<TMaxima>(){

                    public TMaxima createFromParcel(Parcel in){

                        return new TMaxima(in);

                    }

                    public TMaxima[] newArray(int size){

                        return new TMaxima[size];

                    }

                };
    }

    public static class STermica implements Parcelable{

        String value;
        int valor1;
        String descripcion1;

        public String getValue() {
            return value;
        }

        public int getValor1() {
            return valor1;
        }

        public String getDescripcion1() {
            return descripcion1;
        }

        public STermica(Parcel parcel){

            readFromParcel(parcel);

        }

        @Override
        public int describeContents(){

            return 0;

        }

        @Override
        public void writeToParcel(Parcel dest, int flags){

            dest.writeString(value);
            dest.writeInt(valor1);
            dest.writeString(descripcion1);

        }

        public void readFromParcel(Parcel parcel){

            value = parcel.readString();
            valor1 = parcel.readInt();
            descripcion1 = parcel.readString();

        }

        public static final Parcelable.Creator<STermica> CREATOR =
                new Parcelable.Creator<STermica>(){

                    public STermica createFromParcel(Parcel in){

                        return new STermica(in);

                    }

                    public STermica[] newArray(int size){

                        return new STermica[size];

                    }

                };

    }

    public static class TAgua implements Parcelable{

        String value;
        int valor1;

        public String getValue() {
            return value;
        }

        public int getValor1() {
            return valor1;
        }

        public TAgua(Parcel parcel){

            readFromParcel(parcel);

        }

        @Override
        public int describeContents(){

            return 0;

        }

        @Override
        public void writeToParcel(Parcel dest, int flags){

            dest.writeString(value);
            dest.writeInt(valor1);

        }

        public void readFromParcel(Parcel parcel){

            value = parcel.readString();
            valor1 = parcel.readInt();

        }

        public static final Parcelable.Creator<TAgua> CREATOR =
                new Parcelable.Creator<TAgua>(){

                    public TAgua createFromParcel(Parcel in){

                        return new TAgua(in);

                    }

                    public TAgua[] newArray(int size){

                        return new TAgua[size];

                    }

                };
    }

    public static class UvMax implements Parcelable{

        String value;
        int valor1;

        public String getValue() {
            return value;
        }

        public int getValor1() {
            return valor1;
        }
        public UvMax(Parcel parcel){

            readFromParcel(parcel);

        }

        @Override
        public int describeContents(){

            return 0;

        }

        @Override
        public void writeToParcel(Parcel dest, int flags){

            dest.writeString(value);
            dest.writeInt(valor1);

        }

        public void readFromParcel(Parcel parcel){

            value = parcel.readString();
            valor1 = parcel.readInt();

        }

        public static final Parcelable.Creator<UvMax> CREATOR =
                new Parcelable.Creator<UvMax>(){

                    public UvMax createFromParcel(Parcel in){

                        return new UvMax(in);

                    }

                    public UvMax[] newArray(int size){

                        return new UvMax[size];

                    }

                };
    }

}
