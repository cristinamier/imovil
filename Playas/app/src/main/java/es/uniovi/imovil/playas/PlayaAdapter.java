package es.uniovi.imovil.playas;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by cristina on 2/4/17.
 */

public class PlayaAdapter extends BaseAdapter{

    static class ViewHolder {
        public TextView mPlaya;
        public TextView mLocalidad;
        public TextView mPosicion;
        public ImageView mImagen;
    }

    private List<Playa> mPlayas;
    public LayoutInflater mInflater;


    public PlayaAdapter(Context context, List<Playa> playas){

        if(context == null || playas == null) {
            throw new IllegalArgumentException();
        }

        this.mPlayas = playas;
        this.mInflater = LayoutInflater.from(context);

    }

    @Override
    public int getCount() {

        return mPlayas.size();

    }

    @Override
    public Object getItem(int position){

        return mPlayas.get(position);

    }

    @Override
    public long getItemId(int position){

        return 0;

    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View rowView = convertView;
        ViewHolder viewHolder;
        if(rowView == null) {
            rowView = mInflater.inflate(R.layout.list_item_playa, null);
            viewHolder = new ViewHolder();
            viewHolder.mPlaya = (TextView) rowView.findViewById(R.id.textViewPlaya);
            viewHolder.mLocalidad = (TextView) rowView.findViewById(R.id.textViewLocalidad);
            viewHolder.mPosicion = (TextView) rowView.findViewById(R.id.textViewPosicion);
            viewHolder.mImagen = (ImageView) rowView.findViewById(R.id.imageView);
            rowView.setTag(viewHolder);
        } else{
            viewHolder = (ViewHolder) rowView.getTag();

        }

        Playa playa = (Playa) getItem(position);
        viewHolder.mPlaya.setText(playa.getNombre());
        viewHolder.mLocalidad.setText(playa.getLocalidad());
        viewHolder.mPosicion.setText(String.valueOf(position+1));
        if(getCount() >= 7) {
            if (position >= 0 && position <= 2) {
                viewHolder.mPosicion.setTextColor(Color.parseColor("#66CC00"));
            } else if (position >= getCount() - 3 && position <= getCount() - 1) {
                viewHolder.mPosicion.setTextColor(Color.parseColor("#FF0000"));
            } else {
                viewHolder.mPosicion.setTextColor(Color.parseColor("#BBBBBB"));
            }
        }
        else{
            if(position == 0){
                viewHolder.mPosicion.setTextColor(Color.parseColor("#66CC00"));
            }
            else if(position == getCount() - 1){
                viewHolder.mPosicion.setTextColor(Color.parseColor("#FF0000"));
            }
            else{
                viewHolder.mPosicion.setTextColor(Color.parseColor("#BBBBBB"));
            }
        }

        if(playa.getPrediccion() != null){
            Date date = new Date();
            String cielo;
            if(date.getHours() <= 14){
                cielo = playa.getPrediccion().getCieloManana();
            }
            else{
                cielo = playa.getPrediccion().getCieloTarde();
            }
            if(cielo.equals("despejado")) {
                viewHolder.mImagen.setImageResource(R.drawable.imagen_despejado);
            }
            else if(cielo.equals("nuboso")) {
                viewHolder.mImagen.setImageResource(R.drawable.imagen_nuboso);
            }
            else if(cielo.equals("muy nuboso")) {
                viewHolder.mImagen.setImageResource(R.drawable.imagen_muy_nuboso);
            }
            else{
                viewHolder.mImagen.setImageResource(R.drawable.imagen_lluvioso);
            }
        }

        return rowView;

    }


}
