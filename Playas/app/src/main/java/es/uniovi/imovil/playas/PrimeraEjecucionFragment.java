package es.uniovi.imovil.playas;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;

import java.util.Locale;

/**
 * Created by cristina on 29/4/17.
 */

public class PrimeraEjecucionFragment extends Fragment{

    View rootView;
    Spinner spinner;
    Button botonAceptar;
    public static final String PREFERENCES = "preferences";
    public static final String ZONA = "zona";
    private SharedPreferences prefs = null;

    public static PrimeraEjecucionFragment newInstance(){

        PrimeraEjecucionFragment fragment = new PrimeraEjecucionFragment();
        return fragment;

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstance){

        rootView = inflater.inflate(R.layout.primera_ejecucion_fragment, container, false);

        // Inicialización del spinner con las zonas de playa
        spinner = (Spinner) rootView.findViewById(R.id.spinner);
        ArrayAdapter<CharSequence> adaptador = ArrayAdapter.createFromResource(getContext(), R.array.zonas,
                android.R.layout.simple_spinner_item);
        adaptador.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adaptador);



        // Inicialización del editor de SharedPreferences
        prefs = getActivity().getSharedPreferences(PREFERENCES, Context.MODE_PRIVATE);
        final SharedPreferences.Editor prefsEditor = prefs.edit();

        // Cuando se hace clic en el botón Aceptar
        botonAceptar = (Button) rootView.findViewById(R.id.buttonSiguiente);
        botonAceptar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String zona = spinner.getSelectedItem().toString();
                if(Locale.getDefault().getDisplayLanguage().equals("English")){
                    switch(zona){
                        case "East":
                            zona = "Este";
                            break;
                        case "Centre":
                            zona = "Centro";
                            break;
                        case "West":
                            zona = "Oeste";
                            break;
                    }
                }
                prefsEditor.putString(ZONA, zona);
                prefsEditor.commit();
                Intent intent = new Intent(getContext(), MainActivity.class);
                startActivity(intent);
                getActivity().finish();
            }
        });

        return rootView;

    }


}
