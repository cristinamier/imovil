package es.uniovi.imovil.playas;

import android.content.Context;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.HurlStack;
import com.android.volley.toolbox.Volley;

import java.io.InputStream;
import java.security.KeyStore;
import java.security.cert.Certificate;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;

import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManagerFactory;

/**
 * Created by cristina on 21/4/17.
 */

public class SingletonVolley {

    private static SingletonVolley mInstance;
    private RequestQueue mRequestQueue;

    private static Context mCtx;

    private SingletonVolley(Context context) {
        mCtx = context;
        mRequestQueue = getRequestQueue();

    }

    public static synchronized SingletonVolley getInstance(Context context) {
        if (mInstance == null) {
            mInstance = new SingletonVolley(context);
        }
        return mInstance;
    }

    public RequestQueue getRequestQueue() {
        if (mRequestQueue == null) {
            // getApplicationContext() is key, it keeps you from leaking the
            // Activity or BroadcastReceiver if someone passes one in.
            // En esta función, al crear la cola de peticiones, se le pide que obtenga los socketsSSL de una SSLSocketFactory que
            // nosotros suministramos configurada con el certificado adecuado
            mRequestQueue = Volley.newRequestQueue(mCtx.getApplicationContext(), new HurlStack(null, newSslSocketFactory()));
        }
        return mRequestQueue;
    }

    public <T> void addToRequestQueue(Request<T> req) {
        getRequestQueue().add(req);
    }


    private SSLSocketFactory newSslSocketFactory() {
        try {
            // Creamos un CertificateFactory para el tipo de certificado que vamos a usar "X.5009"

            CertificateFactory cf = CertificateFactory.getInstance("X.509");
            // Accedemos al certificado que previamente hemos almacenado en la carpeta de recursos raw de nuestro proyecto

            InputStream in = mCtx.getApplicationContext().getResources().openRawResource(R.raw.certificado);
            Certificate ca;
            try {
                // Inicializamos el certificado a partir de ese archivo
                ca = cf.generateCertificate(in);
                // Lo imprimo por motivos de depuración para ver que está bien leído
                System.out.println("MiAPP  ca="+ ((X509Certificate) ca).getSubjectDN());

            } finally {
                in.close();
            }

            // Creamos un almacen de claves del tipo por defecto
            String keyStoreType = KeyStore.getDefaultType();
            KeyStore keyStore = KeyStore.getInstance(keyStoreType);
            // Esta función se utiliza para leer una determinada clave encriptada con una password
            // En nuestro caso, no es necesario porque vamos a utilizar el certificado creado anteriormente
            keyStore.load(null, null);

            // Añadimos la clave a asociada al certificado creado anteriormente
            keyStore.setCertificateEntry("ca", ca);

            // Creamos un TrustManagerFactory (valores por defecto) que es el que encarga de verificar que se cumplen las condiciones
            // de seguridad con el otro extremo de la comunicación
            String tmfAlgorithm = TrustManagerFactory.getDefaultAlgorithm();
            TrustManagerFactory tmf = TrustManagerFactory.getInstance(tmfAlgorithm);
            // Lo inicializamos con los valores de la clave creada anteriormente basada en el certificado
            tmf.init(keyStore);

            // A partir de ese TrustManagerFactory creamos un SSLSocketFactory que nos suministrará conexiones seguras sobre el
            // transporte "TLS"
            SSLContext context = SSLContext.getInstance("TLS");
            context.init(null, tmf.getTrustManagers(), null);
            SSLSocketFactory sf = context.getSocketFactory();
            return sf;
        } catch (Exception e) {
            throw new AssertionError(e);
        }
    }

}
